
import { Component, OnInit } from '@angular/core';
import { DataMessagesService } from '../services/data-messages.service';
import { Message_Impl } from './message';


@Component({
  selector: 'app-writing',
  templateUrl: './writing.component.html',
  styleUrls: ['./writing.component.scss']
})
export class WritingComponent implements OnInit {
  
  m: Message_Impl;

  constructor(
    private service : DataMessagesService
  ) { }
  
  ngOnInit(): void {
    this.m = {
      title : '',
      content : '',
      isRead: false,
    }
  }


  addNewMessage(){

    this.service.addMessage(this.m);
   
    
    // console.log(this.service.getMessages());
    this.resetMessage();

  };

  // methode qui me sert à reset l'etat de l'objet après avoir terminé de l'instancier
  resetMessage(){
    this.m = {
      title : '',
      content : '',
      isRead: false,
    }
  }
  
}
