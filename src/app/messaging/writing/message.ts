export interface Message_Impl {

    title: string;
    content: string;
    isRead: boolean;

}