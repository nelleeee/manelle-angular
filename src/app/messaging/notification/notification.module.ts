import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InboxComponent } from '../inbox/inbox.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    InboxComponent,
  ]
})
export class NotificationModule { }
