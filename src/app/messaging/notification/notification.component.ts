import { Component, OnInit } from '@angular/core';
import { DataMessagesService } from '../services/data-messages.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  notReadMessages: number;

  constructor(
    private service: DataMessagesService
  ) { }

  ngOnInit(): void {

  // v3)
  this.service
    .getCountOfUnreadMessages()
    .subscribe(numberOfUnreadMessages => this.notReadMessages = numberOfUnreadMessages);



    // v2)
    // this.service.getUnreadMessagesAsObservable()
    //             .subscribe(unreadMessages => this.notReadMessages = unreadMessages.length)



    // v1)
    // this.service
    //   .getSubjectMessages()
    //   .pipe(
    //     map((messages,idx) =>{
    //       return messages.filter((message) => {
    //         return !message.isRead;
    //       }).length
    //     })
    //   ).subscribe((datas) => {
    //     this.notReadMessages = datas
    //   })
  }

}
