import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Message_Impl } from '../writing/message';
import { HttpClient, HttpResponse } from '@angular/common/http';




@Injectable({
  providedIn: 'root'
})
export class DataMessagesService {

 
  private subject = new BehaviorSubject<Message_Impl[]>([]);

  // fait dans le construct pour qu'à l'init de datamessagesservice : accès à la source de donnée

  private messages;

  constructor(
    private http: HttpClient 
  ) { 
     this.getMessagesFromApi();
    }

    getMessagesFromApi(){

      this.http.get<any>('http://localhost:3030/messages')
      .pipe()
      .subscribe(
          (response) => {
            this.subject.next(response)
          },
          (error) => {
            console.log(error);
          },
          () => {
            console.log("complete");
          }
      )
    }

    listOfMessages(){
      return this.subject.asObservable();
    };




    // postMessageToApi(){
    //   this.http.post<any>('https://reqres.in/api/posts', 
    //   { title: 'Angular POST Request Example' }).subscribe(data => {
    //     this.postId = data.id;
    // })
    // }

      
  // getMessages(){
  //   this.getMessagesApi();
  // }


  //demander pq ça fonctioneXD
  addMessage(messageToAdd: Message_Impl){

    this.http.post('http://localhost:3030/messages', messageToAdd)
    .pipe()
    .subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        },
        () => {
          console.log("complete");
        }
    )
    
    
  }

  getSubjectMessages(): Observable<Message_Impl[]>{
    return this.subject.asObservable();
    
  }




  deleteMessageById(id: number){
    this.http.delete<any>(`http://localhost:3030/messages/${id}`)
    .subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        },
        () => {
          console.log("complete");
        }
    )
  }

  // getcountofunreadmessage

  getUnreadMessagesAsObservable(): Observable<Message_Impl[]>{
    return this.subject.pipe(
      map(messages => messages.filter(message => !message.isRead))
    );
  }
 
  getCountOfUnreadMessages(): Observable<number>{
    return this.subject.pipe(
      map(messages => messages.filter(message => !message.isRead)),
      map(unreadMessages => unreadMessages.length)
    );
  }
}
