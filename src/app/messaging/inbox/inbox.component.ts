import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DataMessagesService } from '../services/data-messages.service';
import { Message_Impl } from '../writing/message';



@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {

  constructor(private service: DataMessagesService) { 

  }

  @Input() content = "contenu de du component";

  @Output() dataevent = new EventEmitter();

  messages;

  badge: number;



  ngOnInit(): void {

    this.service.listOfMessages().subscribe((datas) => {
      this.messages = datas;
    })

     this.badge = this.messages.length;
    
  }

  
  

  // change(){
  //   this.title = 'titre modifié on click';
  // }

  // delete() {
  //   this.messages.pop();
  // }

  notifs() {
    this.badge = this.messages.length;
    // console.log(this.messages.length);
  }

  deleteMessage(id: number){
    this.service.deleteMessageById(id);
    console.log(id);
    this.badge = this.messages.length;

    this.service.listOfMessages().subscribe((datas) => {
      this.messages = datas;
    })
  }
  

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
  }
}
