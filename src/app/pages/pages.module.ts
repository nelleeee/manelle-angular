import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { WritingComponent } from '../messaging/writing/writing.component';



@NgModule({
  declarations: [
    DashboardComponent,
    ProfileComponent,
    WritingComponent,
    HeaderComponent,
    SidebarComponent,
    ContentComponent,
    FooterComponent,

  ],
  imports: [
    CommonModule,
  ]
})
export class PagesModule { }
